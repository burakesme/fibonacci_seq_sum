from flask import Flask
from healthcheck import HealthCheck



app = Flask(__name__)



health = HealthCheck(app, "/health")

def calcFiboTerms(fiboTerms, K):
    i = 3
    fiboTerms.append(0)
    fiboTerms.append(1)
    fiboTerms.append(1)

    # Calculate all Fibonacci terms
    # which are less than or equal to K.
    while True:
        nextTerm = (fiboTerms[i - 1] +
                    fiboTerms[i - 2])
        # If next term is greater than K
        # do not push it in vector and return.
        if nextTerm > K:
            return tuple(list(filter(lambda a: a != 0 and a != 1, fiboTerms)))
        fiboTerms.append(nextTerm)
        i += 1


def find_comb(fiboTerms,previous_list, target, last_index):
  if target == 0:
    return [previous_list]
  solutions = []
  for i, value in enumerate(fiboTerms[last_index:len(fiboTerms)]):
    if value > target:
      break
    solution = find_comb(fiboTerms,previous_list + [value], target-value, i+last_index)
    if solution:
      solutions.extend(solution)
  return solutions

@app.route('/')
def index():
    return 'Index Page'


@app.route('/fib/<int:number>')
def send_fibo(number):
    fiboTerms = calcFiboTerms([], number)
    return str(find_comb(fiboTerms,[],number,0))

if __name__ == "__main__":
    app.run(debug=True)